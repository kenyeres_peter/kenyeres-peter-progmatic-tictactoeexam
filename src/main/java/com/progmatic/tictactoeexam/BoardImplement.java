/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author én
 */
public class BoardImplement implements Board {

    ArrayList<Cell> cells = new ArrayList<>();

    public BoardImplement() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cells.add(new Cell(i, j));
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx > 2 || rowIdx < 0 || colIdx > 2 || colIdx < 0) {
            throw new CellException(rowIdx, colIdx, "A megadott mező kívül esik a táblán!");
        }
        for (Cell cell : cells) {
            if (cell.getCol() == colIdx && cell.getRow() == rowIdx) {
                return cell.getCellsPlayer();
            }
        }
        return null;
    }

    @Override
    public void put(Cell cell) throws CellException {
        int cellToReplace = 0;
        if (cell.getCol() > 2 || cell.getCol() < 0 || cell.getRow() > 2 || cell.getRow() < 0) {
            throw new CellException(cell.getRow(), cell.getCol(), "A megadott mező kívül esik a táblán!");
        }
        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getCol() == cell.getCol() && cell.getRow() == cells.get(i).getRow()) {
                if (cells.get(i).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    cellToReplace = i;
                } else {
                    throw new CellException(cells.get(i).getRow(), cells.get(i).getCol(), "A mező már tartlmaz X-et vagy 0-át!");
                }
            }
        }
        cells.set(cellToReplace, cell);

    }

    @Override
    public boolean hasWon(PlayerType p) {

        Cell[][] cellsInAMatrix = new Cell[3][3];
        for (Cell cell : cells) {
            cellsInAMatrix[cell.getRow()][cell.getCol()] = cell;
        }

        for (int i = 0; i < 3; i++) {
            if (cellsInAMatrix[i][0].getCellsPlayer().equals(p)
                    && cellsInAMatrix[i][1].getCellsPlayer().equals(p) && cellsInAMatrix[i][2].getCellsPlayer().equals(p)) {
                return true;
            }
            if (cellsInAMatrix[0][i].getCellsPlayer().equals(p)
                    && cellsInAMatrix[1][i].getCellsPlayer().equals(p) && cellsInAMatrix[2][i].getCellsPlayer().equals(p)) {
                return true;
            }
        }
        if (cellsInAMatrix[0][0].getCellsPlayer().equals(p) && cellsInAMatrix[1][1].getCellsPlayer().equals(p)
                && cellsInAMatrix[2][2].getCellsPlayer().equals(p)) {
            return true;
        }
        if (cellsInAMatrix[0][2].getCellsPlayer().equals(p) && cellsInAMatrix[1][1].getCellsPlayer().equals(p)
                && cellsInAMatrix[2][0].getCellsPlayer().equals(p)) {
            return true;
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        ArrayList<Cell> result = new ArrayList<>();
        for (Cell cell : cells) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                result.add(cell);
            }
        }
        return result;
    }

    @Override
    public ArrayList<Cell> getCells() {
        return cells;
    }

}
