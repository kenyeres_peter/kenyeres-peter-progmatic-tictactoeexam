package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

public class VictoryAwarePlayer extends AbstractPlayer {

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {

        Cell[][] cellsInAMatrix = new Cell[3][3];
        for (Cell cell : b.getCells()) {
            cellsInAMatrix[cell.getRow()][cell.getCol()] = cell;
        }

        int diagonalCount = 0;
        int diagonalCountDownToUp = 0;

        for (int i = 0; i < 3; i++) {
            int horizontalCount = 0;
            int verticalCount = 0;
            if (cellsInAMatrix[i][i].getCellsPlayer().equals(myType)) {
                diagonalCount++;
            }
            if (cellsInAMatrix[cellsInAMatrix.length - i - 1][i].getCellsPlayer().equals(myType)) {
                diagonalCountDownToUp++;
            }
            for (int j = 0; j < 3; j++) {
                if (cellsInAMatrix[i][j].getCellsPlayer().equals(myType)) {
                    horizontalCount++;
                }
                if (cellsInAMatrix[j][i].getCellsPlayer().equals(myType)) {
                    verticalCount++;
                }
            }
            if (horizontalCount == 2) {
                for (int j = 0; j < 3; j++) {
                    if (cellsInAMatrix[i][j].getCellsPlayer().equals(PlayerType.EMPTY)) {
                        Cell resultCell = new Cell(i, j);
                        resultCell.setCellsPlayer(myType);
                        return resultCell;
                    }
                }
            }
            if (verticalCount == 2) {
                for (int j = 0; j < 3; j++) {
                    if (cellsInAMatrix[j][i].getCellsPlayer().equals(PlayerType.EMPTY)) {
                        Cell resultCell = new Cell(j, i);
                        resultCell.setCellsPlayer(myType);
                        return resultCell;
                    }
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            if (diagonalCount == 2) {
                if (cellsInAMatrix[i][i].getCellsPlayer().equals(PlayerType.EMPTY)) {
                    Cell resultCell = new Cell(i, i);
                    resultCell.setCellsPlayer(myType);
                    return resultCell;
                }
            } else if (diagonalCountDownToUp == 2) {
                if (cellsInAMatrix[cellsInAMatrix.length - i - 1][i].getCellsPlayer().equals(PlayerType.EMPTY)) {
                    Cell resultCell = new Cell(cellsInAMatrix.length - i - 1, i);
                    resultCell.setCellsPlayer(myType);
                    return resultCell;
                }
            }
        }
        for (Cell cell : b.getCells()) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                Cell resultCell = new Cell(cell.getRow(), cell.getCol());
                resultCell.setCellsPlayer(myType);
                return resultCell;
            }
        }

        return null;
    }
}
