package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

public class SimplePlayer extends AbstractPlayer {
    
    public SimplePlayer(PlayerType p) {
        super(p);
    }
    
    @Override
    public Cell nextMove(Board b) {
        
        for (int i = 0; i < b.getCells().size(); i++) {
            if (b.getCells().get(i).getCellsPlayer().equals(PlayerType.EMPTY)) {
                Cell resultCell = new Cell(b.getCells().get(i).getRow(), b.getCells().get(i).getCol());
                resultCell.setCellsPlayer(myType);
                return resultCell;
            }
        }
        return null;
    }
    
}
